﻿<# This form was created using POSHGUI.com  a free online gui designer for PowerShell
.NAME
    Untitled
#>
 
Try
{
    Import-Module ActiveDirectory
}
Catch
{
    Write-Host "ActiveDirectory Module could not be loaded. Script has Stopped"
    Exit 1
}


Add-Type -AssemblyName System.Windows.Forms
[System.Windows.Forms.Application]::EnableVisualStyles()

Function GUI{ 

$NewStarterForm                  = New-Object system.Windows.Forms.Form
$NewStarterForm.ClientSize       = '400,253'
$NewStarterForm.text             = "New Starter Form"
$NewStarterForm.TopMost          = $false

$CreateButton                    = New-Object system.Windows.Forms.Button
$CreateButton.text               = "Create"
$CreateButton.width              = 60
$CreateButton.height             = 30
$CreateButton.location           = New-Object System.Drawing.Point(237,198)
$CreateButton.Font               = 'Microsoft Sans Serif,10'

$CancelButton                    = New-Object system.Windows.Forms.Button
$CancelButton.text               = "Cancel"
$CancelButton.width              = 60
$CancelButton.height             = 30
$CancelButton.location           = New-Object System.Drawing.Point(315,198)
$CancelButton.Font               = 'Microsoft Sans Serif,10'

$FirstNameLabel                  = New-Object system.Windows.Forms.Label
$FirstNameLabel.text             = "First Name"
$FirstNameLabel.AutoSize         = $true
$FirstNameLabel.width            = 25
$FirstNameLabel.height           = 10
$FirstNameLabel.location         = New-Object System.Drawing.Point(12,20)
$FirstNameLabel.Font             = 'Microsoft Sans Serif,10'

$SurnameLabel                    = New-Object system.Windows.Forms.Label
$SurnameLabel.text               = "Surname"
$SurnameLabel.AutoSize           = $true
$SurnameLabel.width              = 25
$SurnameLabel.height             = 10
$SurnameLabel.location           = New-Object System.Drawing.Point(12,52)
$SurnameLabel.Font               = 'Microsoft Sans Serif,10'

$DepartmentLabel                 = New-Object system.Windows.Forms.Label
$DepartmentLabel.text            = "Department"
$DepartmentLabel.AutoSize        = $true
$DepartmentLabel.width           = 25
$DepartmentLabel.height          = 10
$DepartmentLabel.location        = New-Object System.Drawing.Point(12,165)
$DepartmentLabel.Font            = 'Microsoft Sans Serif,10'

$SiteLabel                       = New-Object system.Windows.Forms.Label
$SiteLabel.text                  = "Site"
$SiteLabel.AutoSize              = $true
$SiteLabel.width                 = 25
$SiteLabel.height                = 10
$SiteLabel.location              = New-Object System.Drawing.Point(12,122)
$SiteLabel.Font                  = 'Microsoft Sans Serif,10'

$UsernameLabel                   = New-Object system.Windows.Forms.Label
$UsernameLabel.text              = "Username"
$UsernameLabel.AutoSize          = $true
$UsernameLabel.width             = 25
$UsernameLabel.height            = 10
$UsernameLabel.location          = New-Object System.Drawing.Point(12,84)
$UsernameLabel.Font              = 'Microsoft Sans Serif,10'

$FirstNameTextBox                = New-Object system.Windows.Forms.TextBox
$FirstNameTextBox.multiline      = $false
$FirstNameTextBox.width          = 213
$FirstNameTextBox.height         = 20
$FirstNameTextBox.location       = New-Object System.Drawing.Point(136,20)
$FirstNameTextBox.Font           = 'Microsoft Sans Serif,10'

$SurnameTextBox                  = New-Object system.Windows.Forms.TextBox
$SurnameTextBox.multiline        = $false
$SurnameTextBox.width            = 213
$SurnameTextBox.height           = 20
$SurnameTextBox.location         = New-Object System.Drawing.Point(136,52)
$SurnameTextBox.Font             = 'Microsoft Sans Serif,10'

$UserNameTextBox                 = New-Object system.Windows.Forms.TextBox
$UserNameTextBox.multiline       = $false
$UserNameTextBox.width           = 213
$UserNameTextBox.height          = 20
$UserNameTextBox.location        = New-Object System.Drawing.Point(136,84)
$UserNameTextBox.Font            = 'Microsoft Sans Serif,10'

$SiteComboBox                    = New-Object system.Windows.Forms.ComboBox
$SiteComboBox.text               = "Choose a Site..."
$SiteComboBox.width              = 212
$SiteComboBox.height             = 20
@('Head Office','Distribution Centre','Here East','Hong Kong','Store') | ForEach-Object {[void] $SiteComboBox.Items.Add($_)}
$SiteComboBox.location           = New-Object System.Drawing.Point(135,122)
$SiteComboBox.Font               = 'Microsoft Sans Serif,10'

$DepartmentComboBox              = New-Object system.Windows.Forms.ComboBox
$DepartmentComboBox.text         = "Choose a Department"
$DepartmentComboBox.width        = 212
$DepartmentComboBox.height       = 20
@('Tech','Marketing','Buying','Merchandising','Finance','Executive','Customer Care','MyStylist','Private Shopping','Studio','Press','Events','Logistics','Editorial') | ForEach-Object {[void] $DepartmentComboBox.Items.Add($_)}
$DepartmentComboBox.location     = New-Object System.Drawing.Point(135,155)
$DepartmentComboBox.Font         = 'Microsoft Sans Serif,10'

$NewStarterForm.controls.AddRange(@($CreateButton,$CancelButton,$FirstNameLabel,$SurnameLabel,$DepartmentLabel,$SiteLabel,$UsernameLabel,$FirstNameTextBox,$SurnameTextBox,$UserNameTextBox,$SiteComboBox,$DepartmentComboBox))

#region gui events {

$CreateButton.Add_Click({createuser})

$CancelButton.Add_Click({$NewStarterForm.Close()})

#endregion events }

[void]$NewStarterForm.ShowDialog()

}


#Write your logic code here

Function test {

Write-Host "This Button Works"

}

Function createuser {

#Sets the site according to user input, exits script if this cannot be done

$firstname = $FirstNameTextBox.Text
$lastname = $SurnameTextBox.Text
$username = $UserNameTextBox.Text
$site = $SiteComboBox.Text
$department = $DepartmentComboBox.Text

try
{
    if ($site -eq "Head Office")
    {
        $location = "OU=Users,OU=" + $department + ",OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
        $officename = "HO"
        Write-Host "Site set to Head Office"

    } elseif ($site -eq "Distribution Centre") 
    {
        $location = "OU=Users,OU=" + $department + ",OU=DC,OU=Sites,DC=matches,DC=com"
        $officename = "DC"
        Write-Host "Site set to Distribution Centre"

    } elseif ($site -eq "Here East")
    {
        $location = "OU=Users,OU=Here East,OU=Sites,DC=matches,DC=com"
        $officename = "HO"
        Write-Host "Site set to Here East"

    } elseif ($site -eq "Hong Kong")
    {
        $location = "OU=Users,OU=Hong Kong,OU=Sites,DC=matches,DC=com"
        $officename = "HO"
        Write-Host "Site set to Hong Kong"
    } elseif ($site -eq "Store")
    {
        $location = "OU=Users,OU="+ $department + ",OU=Stores,OU=Sites,DC=matches,DC=com"
        
        #To improve on current if statement for store signatire via the officename attribute in AD
        
        if ($department -eq "5 Carlos Place")
        {
            $officename = "CP"

        } elseif ($department -eq "36 Wimbledon")
        {
            $officename = "Wimbledon36"

        } elseif ($department -eq "Hackney")
        {
            $officename = "Hackney"

        }elseif ($department -eq "Ledbury 60")
        {
            $officename = "Ledbury (60)"

        }elseif ($department -eq "Marylebone")
        {
            $officename = "Marylebone (87)"

        }else
        {
            $officename = "HO"
           Write-Host "Site set to Stores but store is not listed, please check Office section for the correct signature!"

        }
            
    }  

}
catch
{
    Write-Host "Location was not set correctly"
    Exit 1

}



#cleans up spaces in user inputs
$firstname = $firstname.Trim()
$lastname = $lastname.Trim()
$username = $username.Trim()

$firstemail = $firstname -replace '\s',''
$lastemail = $lastname -replace '\s',''

#Confirms that there are no foriegn characters in username

$pat = "^[a-zA-Z]+$"

if ($username -match $pat -and $firstemail -match $pat -and $lastemail -match $pat)
{
    Write-Host "No issue with names"

} else
{
    Write-Host "Invalid character in Username, Exiting Script"
    Exit 1
}

#Set ups remaining variables

#$dnsroot = (Get-ADDomain).DNSRoot
$username = $username.ToLower()
$emaildomain = "matchesfashion.com"
$homepath = "\\aws-vplofs02.matches.com\Users$\" + $username
$homedrive = "U:"
$company = "Matches"
#$officename = "HO"
$proxyaddress = "SMTP:" + $firstemail + "." + $lastemail + "@" + $emaildomain
$server = "HO-PPDC01.matches.com"

#Sets Password for user account
#$password = ([char[]]([char]33..[char]95) + ([char[]]([char]97..[char]126)) + 0..9 | sort {Get-Random})[0..8] -join ''
$password = "Welcome" + ((0..9 | sort {Get-Random})[0..2] -join '')

#Sets generated password as a secure password
$pass = ConvertTo-SecureString $password -AsPlainText -Force


#Sets Login Script and modifies the Office Attribute according to Department
try 
{
    if ($department -eq "Buying")
    {
        $script = "Buying.bat"
        Write-Host "Using $department script for new user"
    }
    elseif (($department -eq "Customer Care") -or ($department -eq "Private Shopping"))
    {
        $script = "MyStylist.bat"
        Write-Host "Using $department script for new user"
    }
    elseif ($department -eq "Editorial")
    {
        $script = "Editorial.bat"
        Write-Host "Using $department script for new user"
    }
    elseif ($department -eq "Finance")
    {
        $script = "Finance.bat"
        Write-Host "Using $department script for new user"
    }
    elseif ($department -eq "HR")
    {
        $script = "HR.bat"
        Write-Host "Using $department script for new user"
        $officename = "HR"
    }
    elseif ($department -eq "IT")
    {
        $script = "LoginIT.bat"
        Write-Host "Using $department script for new user"
    }
    elseif ($department -eq "Marketing")
    {
        $script = "Marketing.bat"
        Write-Host "Using $department script for new user"
    }
    elseif ($department -eq "Merchandising")
    {
        $script = "Merch.bat"
        Write-Host "Using $department script for new user"
    }
    elseif ($department -eq "Studio")
    {
        $script = "PhotoStudio.bat"
        Write-Host "Using $department script for new user"
    }
    elseif ($department -eq "RAEY")
    {
        $script = "login.bat"
        $officename = "RAEY"
        Write-Host "Set to use $officename Signature"
    }
    else
    {
        $script = "login.bat"
        Write-Host "Using default script for new user"
    }
}
catch
{ Write-Host "There was an issues with assigning a login script. Please do this manaully" } 

#Checks if user already exists in AD
try { $exist = Get-ADUser -LDAPFilter $username -Server $server}
catch { }


if (!$exist) 
{
    #try 
    #{
        #Creates the user account in AD 

        Write-Host "Creating User Account..." 

        New-ADUser -name ($firstname + " " + $lastname) `
            -SamAccountName $username `
            -AccountPassword $pass `
            -GivenName $firstname `
            -Surname $lastname `
            -DisplayName ($firstname + " " + $lastname) `
            -Title $title `
            -Description $title `
            -Department $department `
            -UserPrincipalName ($firstemail + "." + $lastemail + "@" + $emaildomain) `
            -EmailAddress ($firstemail + "." + $lastemail + "@" + $emaildomain) `
            -HomeDirectory $homepath `
            -HomeDrive $homedrive `
            -ScriptPath $script `
            -Company $company `
            -Office $officename `
            -ChangePasswordAtLogon $true `
            -Enabled $true `
            -server $server


        #Pulls the Distinguished Name for the newly created AD account
        $dn = (Get-ADUser $username -Server $server).DistinguishedName

        # Adds Proxy address for Office 365 Sync
        try { $dn | Set-ADUser -Add @{proxyAddresses = $proxyaddress } -ErrorAction Stop -Server $server}
        catch { Write-Host "Could not create the proxy address $proxyaddress in attributes. Please perform this manually" }

        #Confirms OU location exists in AD before moving account
        if ([adsi]::Exists("LDAP://$($location)"))
        {
        
        Move-ADObject -Identity $dn -TargetPath $location -Server $server

        }
        Else
        {
        Write-Host "Location was not found, please move account manually"
        }
        
        #Section to add user to groups
        
        
        
        
        
        #Feeds back user details to TSA

        Write-Host "The User Account for $firstname $lastname has been created."
        Write-Host "Log in details is as follows:"
        Write-Host "Username: $username"
        Write-Host "Password: $password"
        Write-Host "Email: $firstemail.$lastemail@$emaildomain"
        Write-Host "The User's Password is set to change on their first login"
        Write-Host "Please add user to groups via AD" 

    #}
    #Catch
    #{
    #    Write-Host "Something Went Wrong"
    #}

} 
else 
{
    Write-Host "The User $username already exists! Please try another username"
}

}

GUI