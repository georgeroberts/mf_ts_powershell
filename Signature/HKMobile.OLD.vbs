
'Switch for dev mode (true, false)
DevMode = false

'Default links for office
sURLWWW = "http://www.matchesfashion.com"
sURLFacebook = "http://www.facebook.com/matchesfashion"
sURLTwitter = "http://twitter.com/matchesfashion"
sURLYoutube = "https://www.youtube.com/user/matchesfashion"
sURLInstagram = "https://www.instagram.com/matchesfashion"
sURL_Googleplus = "https://plus.google.com/+MATCHESFASHION"
sURL_Pininterest = "http://pinterest.com/matchesfashion"

'Setup script functions for calls to file system
Set WshShell = WScript.CreateObject("WScript.Shell")
Set FileSysObj = CreateObject("Scripting.FileSystemObject")
strQuteChr = chr(34)

if DevMode then
	msgbox "Devmode is ON"
	strDesktop = WshShell.SpecialFolders("Desktop")
	SigFolder = strDesktop & "\sigs\"
	SigFile = SigFolder & "sig_dev.htm"
        SigFileTxt = SigFolder & "sig_dev.txt"
else
	'create the objects and set the initial vars
	Set objADSysInfo = CreateObject("ADSystemInfo")
	Set UserObj = GetObject("LDAP://" & objADSysInfo.UserName)
	strAppData = WshShell.ExpandEnvironmentStrings("%APPDATA%")
	SigFolder = StrAppData & "\Microsoft\Signatures\"
	SigFile = SigFolder & UserObj.sAMAccountName & ".htm"
	SigFileTxt = SigFolder & UserObj.sAMAccountName & ".txt"
End If

'check the existence of the sig dir, if not there create it
if not FileSysObj.FolderExists(SigFolder) then
	FileSysObj.CreateFolder(SigFolder)
end if

if DevMode then
	sUserName = "johnh"
	sFullName = "John Hudson"
	sPosition = "Linux System Administrator"
	sMobile = "07775557880"
	sEmail = "john.hudson@matchesfashion.com"
	sCompany = "Matches Fashion"
	sOfficePhone = "0845 6025 612"
	sOfficeFax = "020 7622 5843"
else
	'Get User Data from Active Directory
	sUserName = UserObj.sAMAccountName
	sFullName = UserObj.displayname
	sPosition = UserObj.title
	sMobile = UserObj.mobile
	sEmail = UserObj.mail
	sCompany = UserObj.company
'	sOfficePhone = UserObj.telephoneNumber
	sOfficePhone = "0845 6025 612"
	sOfficeFax = UserObj.facsimileTelephoneNumber
	sIphone = UserObj.ipPhone
	
end if

'drop mobile number if personal
if left(sMobile,1) = "p" then sMobile = ""

'create the htm file and start adding content
Set CreateSigFile = FileSysObj.CreateTextFile (SigFile, 8, False)
Set CreateSigFileTxt = FileSysObj.CreateTextFile (SigFileTxt, 8, False)

'html head
CreateSigFile.WriteLine "<!DOCTYPE HTML PUBLIC " & """-//W3C//DTD HTML 4.0 Transitional//EN" & """>"
CreateSigFile.WriteLine "<HTML><HEAD><TITLE>Outlook Signature</TITLE>"
CreateSigFile.WriteLine "<style type= text/css>"
CreateSigFile.WriteLine ".name { font-family: Arial; font-size:10.5pt}"
CreateSigFile.WriteLine ".position { font-family: Arial; font-size:10.5pt}"
CreateSigFile.WriteLine ".disclaimer { font-family: Arial; font-size:9pt }"
CreateSigFile.WriteLine ".address { font-family: Arial; font-size:10.5pt }"
CreateSigFile.WriteLine ".default-table {  }"
CreateSigFile.WriteLine "</style></HEAD>"



'html body
CreateSigFile.WriteLine "<BODY>"
CreateSigFile.writeLine "<table width=" & "325""" & ">"
CreateSigFile.writeLine "<tr>"
CreateSigFile.writeLine "<td width=" & """325""" & ">"
CreateSigFile.writeLine "<strong class=" & """name"""  & ">" & "<font color=" & """black""" & ">"& sFullName & "</strong>"
CreateSigFile.writeLine "</td>"
CreateSigFile.writeLine "</tr>"
CreateSigFile.writeLine "<tr>"
CreateSigFile.writeLine "<td colspan=" & """6""" & ">"
CreateSigFile.writeLine "<span class=" &  """position""" & ">"& "<font color=" & """black""" & ">"& sPosition & "</span></br>"
CreateSigFile.writeLine "</td>"
CreateSigFile.writeLine "</tr>"
CreateSigFile.writeLine "<tr height=" & """50""" & ">"
CreateSigFile.writeLine "<td colspan=" & """6""" & ">"
CreateSigFile.writeLine "<p><a href=" & """http://www.matchesfashion.com""" &">" & "<img src=" & """http://assets.matchesfashion.com.s3.amazonaws.com/images/email_images/matchesfashion-logo.png""" & "alt=" & """""" &  "width=" & """305""" & "></a></p>"
CreateSigFile.writeLine "</td>"
CreateSigFile.writeLine "</tr>"
CreateSigFile.writeLine "<tr>"
CreateSigFile.writeLine "<td colspan=" & """6""" & ">"
CreateSigFile.writeLine "<span class=" & """address""" &  ">" & "<font color=" & """black""" & ">" & "<strong>A</strong>" & " 12th Floor, 239 Hennessy Road, <br/>Wanchai, Hong Kong</span>"
CreateSigFile.writeLine "</td>"
CreateSigFile.writeLine "</tr>"
CreateSigFile.writeLine "<tr>"
CreateSigFile.writeLine "<td colspan=" & """6""" & ">"
CreateSigFile.writeLine "<span class=" & """name"""  & ">" & "<font color=" & """black""" & ">" &" <strong>T</strong> +852.2336.3077 " & sIphone & " <br><strong>M </strong>" & sMobile  
CreateSigFile.writeLine "</td>"
CreateSigFile.writeLine "</tr>"
CreateSigFile.writeLine "<tr>"
CreateSigFile.writeLine "<td colspan=" & """6""" & " style=" & "padding-bottom:" & """15px;""" & ">"
CreateSigFile.writeLine "<span class=" & """name""" & "> "  & "<font color=" & """black""" & "><strong>W</strong></font color><a href=" & """http://www.matchesfashion.com""" & " style=" & """text-decoration:none""" & "><font color=" & """black""" & "> www.matchesfashion.com</font></a></span>"
CreateSigFile.writeLine "</td>"
CreateSigFile.writeLine "</tr>"
CreateSigFile.writeLine "</table>"
CreateSigFile.writeLine "<table width=" & """325""" &">"
CreateSigFile.writeLine "<tr>"
CreateSigFile.writeLine "<td>"

CreateSigFile.writeLine "<a href=" & """http://articles.matchesfashion.com.s3.amazonaws.com/redirector/index.html""" & ">  <img src=" & """http://assets.matchesfashion.com/content/img/MAILERS/2016/INTERNAL_SIGNATURE_IMAGES/Internal-Signature-AW16-crop_NEW_02_FINAL.jpg""" &  "width=" & """309""" & "height=" & """80""" & "></a></td>"

CreateSigFile.writeLine "</tr>"
CreateSigFile.writeLine "</table>"
CreateSigFile.writeLine "<table width=" & """325""" &">"
CreateSigFile.writeLine "<tr>"


CreateSigFile.writeLine "<td>"

CreateSigFile.writeLine "<a href=" & """http://www.facebook.com/matchesfashion""" & ">  <img src=" & """http://assets.matchesfashion.com/content/img/MAILERS/2016/NEW_FOOTER_WITH_ICONS/Internal-Signature-AW16-crop-CROPPEDNEW_03.jpg""" &  "width=" & """40""" & "></a></td>"
CreateSigFile.writeLine "<td>"
CreateSigFile.writeLine "<a href=" & """http://twitter.com/matchesfashion""" & ">  <img src=" & """http://assets.matchesfashion.com/content/img/MAILERS/2016/NEW_FOOTER_WITH_ICONS/Internal-Signature-AW16-crop-CROPPEDNEW_05.jpg""" &  "width=" & """40""" & "></a></td>"
CreateSigFile.writeLine "<td>"
CreateSigFile.writeLine "<a href=" & """http://pinterest.com/matchesfashion""" & ">  <img src=" & """http://assets.matchesfashion.com/content/img/MAILERS/2016/NEW_FOOTER_WITH_ICONS/Internal-Signature-AW16-crop-CROPPEDNEW_07.jpg""" &  "width=" & """40""" & "></a></td>"
CreateSigFile.writeLine "<td>"
CreateSigFile.writeLine "<a href=" & """https://www.instagram.com/matchesfashion""" & ">  <img src=" & """http://assets.matchesfashion.com/content/img/MAILERS/2016/NEW_FOOTER_WITH_ICONS/Internal-Signature-AW16-crop-CROPPEDNEW_09.jpg""" &  "width=" & """40""" & "></a></td>"
CreateSigFile.writeLine "<td>"
CreateSigFile.writeLine "<a href=" & """https://plus.google.com/+MATCHESFASHION""" & ">  <img src=" & """http://assets.matchesfashion.com/content/img/MAILERS/2016/NEW_FOOTER_WITH_ICONS/Internal-Signature-AW16-crop-CROPPEDNEW_11.jpg""" &  "width=" & """40""" & "></a></td>"
CreateSigFile.writeLine "<td>"
CreateSigFile.writeLine "<a href=" & """https://www.youtube.com/user/matchesfashion""" & ">  <img src=" & """http://assets.matchesfashion.com/content/img/MAILERS/2016/NEW_FOOTER_WITH_ICONS/Internal-Signature-AW16-crop-CROPPEDNEW_13.jpg""" &  "width=" & """40""" & "></a></td>"
CreateSigFile.writeLine "</tr>"
CreateSigFile.writeLine "</table>"
CreateSigFile.writeLine "<br>"
CreateSigFile.writeLine "<span class=" & """disclaimer""" &  "<font color=" & """black""" & ">" &" <p>  This e-mail is intended solely for the above-mentioned recipient and it may contain confidential or privileged information. If you have received it in error, please notify us immediately by e-mail return and delete the e-mail. You must not copy, distribute, disclose or take any action in reliance on it. MATCHESFASHION.COM does not guarantee security, accuracy or completeness of this Email and attachments nor does it accept liability for any error omissions,viruses or timeliness arising from its transmission. Any liability for viruses is excluded to the fullest extent permitted by law.  </p></span>"
CreateSigFile.WriteLine "</BODY>"

'close the signature file
CreateSigFile.Close
CreateSigFileTxt.WriteLine sFullName
CreateSigFileTxt.WriteLine sPosition
if not sMobile = "" then
	CreateSigFileTxt.WriteLine sMobile & "    " & LCase(sEmail)
else
	CreateSigFileTxt.WriteLine LCase(sEmail)
end if
CreateSigFileTxt.WriteLine ""
CreateSigFileTxt.WriteLine sCompany
CreateSigFileTxt.WriteLine "Address"
CreateSigFileTxt.WriteLine sOfficePhone
CreateSigFileTxt.WriteLine sURLWWW
CreateSigFileTxt.WriteLine ""
CreateSigFileTxt.WriteLine ""

'close the txt signature file
CreateSigFileTxt.Close

if not DevMode then
        'Set the signature as default for new and reply
	Set objWord = CreateObject("Word.Application")
	Set objSignatureObjects = objWord.EmailOptions.EmailSignature
	objSignatureObjects.NewMessageSignature = sUserName
	objSignatureObjects.ReplyMessageSignature = sUserName
	objWord.Quit
End If
