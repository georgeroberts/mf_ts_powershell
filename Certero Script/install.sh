#!/bin/bash

#variables

certeroURL="http://ho-vpassetstudio.matches.com/CerteroWebApp/Client/Mac/csinvcli64.run"
certero="/tmp/cs64.run"

# Check Connection to www.google.com
/usr/bin/curl -D- -o /dev/null -s http://ho-vpassetstudio.matches.com
if [[ $? != 0 ]]; then
	echo "Certero not reachable. Check you are on the Matches Network"
	exit $?
# Check if Chrome is Already Installed, If not Installation will Continue
	elif [ -e /Library/assetstudio/client/AssetStudio\ Client.app ]; then
			echo "Certero Already Present, Skipping Installation"
			exit 1
		else
			echo "Cetero not found, Downloading & Installing"
			/usr/bin/curl -o "$certero" "$certeroURL"
      chmod +x "$certero"
      "$certero" -- ho-vpassetstudio.matches.com
      exit 0
fi
