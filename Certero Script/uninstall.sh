#!/bin/sh
#AssetStudio Client uninstall script for Mac OS X.

## Only root can run the script
if [ "$USER" != "root" ]
then
	echo "Please run the uninstall script as the root user."
	exit 1
fi

## Global variables
PRODUCTDIR="/Library/assetstudio"
LAUNCHDAEMONDIR="/Library/LaunchDaemons"

## Clear the terminal
clear

## Welcome message
echo "Welcome to the AssetStudio Client Uninstall Wizard for Mac OS X."

## Stop the client daemon
echo "Stopping client daemon..."
launchctl stop com.certero.assetstudio
sleep 5s	

## Unload the client daemon
echo "Unloading client from launchd..."
launchctl unload $LAUNCHDAEMONDIR/com.certero.assetstudio.plist

## Remove daemon auto-start configuration
echo "Removing launchd configuration..."
rm $LAUNCHDAEMONDIR/com.certero.assetstudio.plist

## Kill the taskbar client
killall "AssetStudio Client"
rm /Library/LaunchAgents/com.certero.AssetStudio-Client.peruser.plist

## Remove installation
echo "Removing files..."
rm -rf $PRODUCTDIR

echo "Done."
exit 0

#End of script

