#!/bin/sh

## Pass the credentials for an admin account that is authorized with FileVault 2
adminName=$4
adminPass=$5

if [ "${adminName}" == "" ]; then
echo "Username undefined. Please pass the management account username in parameter 4"
exit 1
fi

if [ "${adminPass}" == "" ]; then
echo "Password undefined. Please pass the management account password in parameter 5"
exit 2
fi

## Get the logged in user's name
userName=`stat -f%Su /dev/console`

## This first user check sees if the logged in account is already authorized with FileVault 2
adminCheck=$(sudo fdesetup list | grep -F $adminName)
if [ "$adminCheck" != "" ]; then
echo "Admin is already added to the FileVault 2 list."
exit 3
fi


## Check to see if the encryption process is complete
encryptCheck=`fdesetup status`
statusCheck=$(echo "${encryptCheck}" | grep "FileVault is On.")
expectedStatus="FileVault is On."
if [ "${statusCheck}" != "${expectedStatus}" ]; then
echo "The encryption process has not completed, unable to add user at this time."
echo "${encryptCheck}"
exit 4
fi

## Get the logged in user's password via a prompt
echo "Prompting ${userName} for their login password."

USERPASS=$(osascript -e '
tell application "Finder"
   display dialog "Enter your password please to enable FileVault" default answer "" with hidden answer
    set USERPASS to the (text returned of the result)
end tell')


echo "Adding user to FileVault 2 list."

# create the plist file:
echo '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
<key>Username</key>
<string>'$userName'</string>
<key>Password</key>
<string>'$USERPASS'</string>
<key>AdditionalUsers</key>
<array>
    <dict>
        <key>Username</key>
        <string>'$adminName'</string>
        <key>Password</key>
        <string>'$adminPass'</string>
    </dict>
</array>
</dict>
</plist>' > /tmp/fvenable.plist

# now enable FileVault
fdesetup add -inputplist < /tmp/fvenable.plist

## This second user check sees if the logged in account was successfully added to the FileVault 2 list
adminCheck=`fdesetup list | awk -v usrN="$adminName" -F, 'index($0, usrN) {print $1}'`
if [ "${adminCheck}" != "${adminName}" ]; then
echo "Failed to add user to FileVault 2 list."
  ## clean up
  if [[ -e /tmp/fvenable.plist ]]; then
      rm /tmp/fvenable.plist
  fi
exit 5
fi

echo "${userName} has been added to the FileVault 2 list."

## clean up
if [[ -e /tmp/fvenable.plist ]]; then
    rm /tmp/fvenable.plist
fi
exit 0
