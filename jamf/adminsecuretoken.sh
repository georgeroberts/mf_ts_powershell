#!/bin/sh

expectedStatus="Secure token is ENABLED for user admin"
token=$(sysadminctl -secureTokenStatus admin | grep -F "${expectedStatus}")
## expectedStatus="Secure token is ENABLED for user admin"

if [ "${token}" == "${expectedStatus}" ]; then
  echo "<result>Enabled</result>"
else
  echo "<result>Disabled</result>"
fi
