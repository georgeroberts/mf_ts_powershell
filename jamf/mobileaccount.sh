#!/bin/sh

if [[ $(dsconfigad -show | awk '/Create mobile account/{print $NF}') == "Enabled" ]]; then
     echo "<result>Enabled</result>"
else
     echo "<result>Disabled</result>"
fi
