#!/bin/bash
#Install and cleans up High Sierra to Mac
#Will only install if files are avaliable in /private/var/tmp and app is not in /Applications

#Variables

currentUser=$(stat -f "%Su" /dev/console)

if [ ! -e /private/var/tmp/macOS_High_Sierra.dmg ] || [ ! -e /private/var/tmp/macOS_High_Sierra.002.dmgpart ] || [ ! -e /private/var/tmp/macOS_High_Sierra.003.dmgpart ]; then
  echo "One of the dmg files is missing"
  exit $1

  elif [ -e /Applications/Install\ macOS\ High\ Sierra.app ]; then
    echo "High Sierra is already avaliable in Applications folder"
    exit $2
  else
    echo "mounting and installing High Sierra..."
    /usr/bin/hdiutil attach /private/var/tmp/macOS_High_Sierra.dmg -nobrowse -noverify -noautoopen
    /usr/bin/sudo cp -R /Volumes/macOS\ High\ Sierra/Applications/Install\ macOS\ High\ Sierra.app /Applications
    /bin/sleep 10m
    /usr/bin/hdiutil unmount /Volumes/macOS\ High\ Sierra

    /bin/chmod -R +x /Applications/Install\ macOS\ High\ Sierra.app

    echo "High Sierra has been added to the Applications folder"
    exit 0

fi
