$MySearchBase ="OU=DEV,OU=Platform Security Groups,DC=matches,DC=com"
$MyGroupList = get-adgroup -Filter 'GroupCategory -eq "Security" -and GroupScope -eq "Universal"' -SearchBase "$MySearchBase"
$MyGroupList.name
$MyGroupList | Set-ADGroup -GroupScope Global
