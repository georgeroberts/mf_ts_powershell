$OU = 'OU=INT,OU=DEV,OU=Platform Security Groups,DC=matches,DC=com'
$Path = 'C:\Data.csv'

$Groups =  Get-ADGroup -Filter * -SearchBase $OU
$Data = foreach ($Group in $Groups) {
    Get-ADGroupMember -Identity $Group -Recursive | Select-Object @{Name='Group';Expression={$Group.Name}}, @{Name='Member';Expression={$_.Name}}
}
$Data | Export-Csv -Path $Path -NoTypeInformation
