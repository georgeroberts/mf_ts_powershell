﻿# Import active directory module for running AD cmdlets
Import-module ActiveDirectory

#Store the data from UserList.csv in the $List variable
$List = Import-CSV "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\newusers.csv"

$server = "HO-PPDC01.matches.com"

#Loop through user in the CSV
ForEach ($User in $List)
{

$username = $User.username
$department = $User.department

$username = $username.Trim()
$username = $username -replace '\s',''

#Add the user to the TestGroup1 group in AD

#Add-ADGroupMember -Identity DC_Packers -Members $User.username
Add-ADGroupMember -Identity sec_navprd_tsrdp -Members $username -server $server
Add-ADGroupMember -Identity RemoteDesktopUsers -Members $username -server $server
Add-ADGroupMember -Identity DC_All -Members $username -server $server

if ($department -eq "Packing") {
    Add-ADGroupMember -Identity DC_Packers -Members $username -server $server
    }
elseif ($department -eq "QC") {
    Add-ADGroupMember -Identity DC_Packers -Members $username -server $server
    }

elseif ($department -eq "Picking") {
    Add-ADGroupMember -Identity DC_Picking -Members $username -server $server
    }
elseif ($department -eq "GoodsIn") {
    Add-ADGroupMember -Identity "Dc Goods In Team" -Members $username -server $server
    }
elseif ($department -eq "OrderManagement") {
    Add-ADGroupMember -Identity Dep_Ordermanagement -Members $username -server $server
    Add-ADGroupMember -Identity SSRS_Logistics -Members $username -server $server
    Add-ADGroupMember -Identity SSRS_Operations -Members $username -server $server
    }


Write-Host -ForegroundColor Green "User $username completed"

}