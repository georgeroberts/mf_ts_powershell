﻿Import-Module ActiveDirectory

$username = Read-Host -Prompt 'What is the username of the leaver?'
$lastdate = Read-Host -Prompt 'The last date of the user (in the format DD/MM/YYYY)'
$server = "HO-PPDC01.matches.com"

$expirationdate = $lastdate + " 5:30PM"

$fullname = (Get-ADUser -Identity $username -Server $server).name

Set-ADAccountExpiration -Identity $username -DateTime $expirationdate -Server $server

Write-Host "The account for $fullname has been set to expire at $expirationdate " 