﻿#########################################################################################
# Created By Karl Newman-Smart (karlns)
# Script to Create new users within MatchesFashion using CSV file
# Please Check CSV path before running!
#
# Ver 1.0
# - Imports modules and captures user information
# - Creates user in HO OU with all standard attributes filled in AD
# - Moves user to correct OU according to department for HO
# - Confirms location is valid before moving, user account is created in global default OU
# - Sets proxy address once user account is created
# - Sets login script according to department
# Ver 1.1
# - Expanded to Create users in HO, DC, HE and HK OUs in AD
# - Cleans up spaces in first and last name
# - Sets Office Attribute according to Site
# - Sets to generate 9 character password
# - Enables account and sets user to change password on first logon
# Ver 1.2
# - Changed output to one line per user
# - Commented out several Write-Host lines
# -
#########################################################################################

#Loads Active Directory Module. Script will stop if this module cannot be loaded.

Try
{
    Import-Module ActiveDirectory
}
Catch
{
    Write-Host "ActiveDirectory Module could not be loaded. Script has Stopped"
    Exit 1
}

$time = Get-Date -UFormat %d%m%y_%H%M

$csvinput = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\dcnewusers.csv"
$csvoutput = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\dcusers_$time.csv"

$csv = Import-CSV $csvinput

$array = @()

$csv | ForEach-Object {


    $site = $_.site
    $department = $_.department
    $location = "OU=Users,OU=" + $department + ",OU=DC,OU=Sites,DC=matches,DC=com"
    
    #Section would need to be expaned
    $officename = $_.site
    #Section end


    #To enter details regarding new starter

    $firstname = $_.firstname
    $lastname = $_.lastname
    $title = $_.title
    $username =$_.username
    $id = $_.employeeid

    #cleans up spaces in user inputs
    $firstname = $firstname.Trim()
    $lastname = $lastname.Trim()
    $username = $username.Trim()
    $id = $id.Trim()

    $firstemail = $firstname -replace '\s',''
    $lastemail = $lastname -replace '\s',''

    #Set ups remaining variables

    $username = $username.ToLower()
    $emaildomain = "matchesfashion.com"
    $homepath = "\\aws-vplofs02.matches.com\Users$\" + $username
    $homedrive = "U:"
    $company = "Matches"
    $proxyaddress = "SMTP:" + $firstemail + "." + $lastemail + "@" + $emaildomain
    $server = "HO-PPDC01.matches.com"

    #Sets Password for user account
    #$password = ([char[]]([char]33..[char]95) + ([char[]]([char]97..[char]126)) + 0..9 | sort {Get-Random})[0..8] -join ''
    $password = "Welcome" + ((0..9 | sort {Get-Random})[0..2] -join '')


    #Sets generated password as a secure password
    $pass = ConvertTo-SecureString $password -AsPlainText -Force

    #Sets Login Script and modifies the Office Attribute according to Department
    $script = "login.bat"

    #Checks if user already exists in AD
    try { $exist = Get-ADUser -LDAPFilter $username -Server $server}
    catch { }


    if (!$exist)
    {

        try
        {
            #Creates the user account in AD

            New-ADUser -name ($firstname + " " + $lastname) `
                -SamAccountName $username `
                -AccountPassword $pass `
                -GivenName $firstname `
                -Surname $lastname `
                -DisplayName ($firstname + " " + $lastname) `
                -Title $title `
                -Description $title `
                -Department $department `
                -UserPrincipalName ($firstemail + "." + $lastemail + "@" + $emaildomain) `
                -EmailAddress ($firstemail + "." + $lastemail + "@" + $emaildomain) `
                -ScriptPath $script `
                -Company $company `
                -Office $officename `
                -ChangePasswordAtLogon $false `
                -Enabled $true `
                -EmployeeID $id `
                -server $server


            #Pulls the Distinguished Name for the newly created AD account
            $dn = (Get-ADUser $username -Server $server).DistinguishedName

            # Adds Proxy address for Office 365 Sync
            if ($emailcreation -eq "Yes"){

                try { $dn | Set-ADUser -Add @{proxyAddresses = $proxyaddress } -ErrorAction Stop -Server $server}
                catch { Write-Host "Could not create the proxy address $proxyaddress in attributes. Please perform this manually" }
            } else {
            Write-host "Proxy address not created for user $firstname $lastname"
            }

            #Confirms OU location exists in AD before moving account
            if ([adsi]::Exists("LDAP://$($location)"))
            {

            Move-ADObject -Identity $dn -TargetPath $location -Server $server

            }
            Else
            {
            Write-host "User account for user $firstname $lastname not moved to OU $department. Please check that $department is a valid location"
            }
            # Feeds back user details to TSA

            $credentials = New-Object PSObject -Property @{
                Firstname = $firstname
                Lastname = $lastname
                Username = $username
                Password = $password
                Created = "Y"

                }
            $array += $credentials

        }
        Catch
        {
            $credentials = New-Object PSObject -Property @{
                Firstname = $firstname
                Lastname = $lastname
                Username = $username
                Password = "NA"
                Created = "N"

                }
            $array += $credentials
        }

    }
    else
    {
        $credentials = New-Object PSObject -Property @{
                Firstname = $firstname
                Lastname = $lastname
                Username = $username
                Password = "NA"
                Created = "N"

                }
            $array += $credentials
    }

}

$array | Export-Csv -Path $csvoutput
