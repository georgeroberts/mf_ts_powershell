﻿Import-Module ActiveDirectory

$searchOU = "OU=Distribution_Group,OU=Sites,DC=matches,DC=com"

$server = "HO-PPDC01.matches.com"

$groups = Get-ADGroup -Filter 'GroupCategory -eq "Distribution"' -SearchBase $searchOU -server $server
#$groups = Get-ADGroup -Filter 'name -eq "dep_it"' -SearchBase $searchOU -server $server

#Write-Output $groups.Name

$time = $time = Get-Date -UFormat %d%m%y_%H%M
$csv = "C:\temp\distributiongroups_" + $time + ".csv"

$output = foreach ($group in $groups) {
    
    #$groupDN = $group.DistinguishedName
        
    $results = Get-ADGroupMember -Identity $group.DistinguishedName -Server $server -Recursive | Get-ADUser -Server $server -Properties name

    ForEach ($result in $results){
        
        New-Object PSObject -Property @{
            GroupName = $group.Name
            Username = $result.name
            #ObjectClass = $r.objectclass
            #DisplayName = $r.displayname
            }
        }
    #Get-ADGroupMember -Identity $groupDN -server $server | select name | Export-Csv -Path $csv -Append

    }

$output | Export-Csv -path $csv
