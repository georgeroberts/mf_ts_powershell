﻿#change searchbase to OU/CN
Get-ADUser -Filter * -SearchBase "DC=matches,DC=com" -ResultPageSize 0 -Property CN, Description, LastLogonTimestamp |
    Select-Object -Property CN, samAccountName, Description, @{ n = "LastLogonDate"; e = { [datetime]::FromFileTime( $_.lastLogonTimestamp ) } } |
    Sort-Object -Property CN, samAccountName, Description, LastLogonDate |
    #change output below
    Export-CSV -NoTypeInformation "C:\temp\lastlogonusers.csv"