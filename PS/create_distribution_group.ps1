﻿Import-Module ActiveDirectory

$name = Read-Host -prompt "name of group"
$accountname = Read-Host -prompt "account name of group. This will also be used for the email address"
$description = Read-Host -prompt "description of group" 

$email = $accountname + "@matchesfashion.com"
$proxyaddress = "SMTP:" + $email

$description = $description + "Created by " + $env:UserName.ToString()

$server = "HO-PPDC01.matches.com"


New-ADGroup `
    -name $name `
    -DisplayName $name `
    -SamAccountName $accountname `
    -GroupCategory Distribution `
    -GroupScope Global `
    -Path "OU=Distribution_Group,OU=Sites,DC=matches,DC=com" `
    -Description $description `
    -Server $server `

#Pulls the Distinguished Name for the newly created AD account
        $dn = (Get-ADgroup $accountname -Server $server).DistinguishedName

        # Adds Proxy address for Office 365 Sync
        try { $dn | Set-ADGroup -Add @{proxyAddresses = $proxyaddress; mail = $email } -ErrorAction Stop -Server $server}
        catch {  }
    
    