﻿Import-Module ActiveDirectory

$ADServer = "HO-PPDC01"

$csv = Import-Csv C:\CSV\jobtitle.csv
$csv | ForEach-Object { 
        Set-ADUser -Identity $_.username -Title $_.jobtitle -Description $_.jobtitle -Server $ADServer
}
