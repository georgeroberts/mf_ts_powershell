﻿Import-Module ActiveDirectory

$dn = Read-Host -Prompt "DN of OU"

get-aduser -SearchBase $dn -Filter * -Properties samAccountName, Displayname, EmailAddress, Department, Title | select samAccountName, DisplayName, EmailAddress, Department, Title | Export-CSV "C:\temp\csvexportslack.csv"