﻿Import-Module ActiveDirectory

$server = "HO-PPDC01.matches.com"

$username = Read-Host -Prompt 'Enter the Username of the Leaver'

#$csvpath = "C:\Users\karlns.adm\OneDrive - Matchesfashion.com\Documents\CSV\DCnewusers140818.csv"
#$csv = Import-CSV $csvpath

$disableddn = "OU=Ex-Employees,OU=Shared Mailboxes,OU=Generic Accounts With Email,OU=Sites,DC=matches,DC=com"

#$csv | ForEach-Object {

    #$username = $_.samAccountName
    $user = Get-ADUser -Identity $username -Properties MemberOf,info -Server $server
    $dngroups = $user.MemberOf
    $userdn = $user.DistinguishedName
    $groups = @()

    foreach ($dngroup in $dngroups) 
        {
        $groupname = (Get-ADGroup -Identity $dngroup -Server $server).name
        Remove-ADGroupMember -identity $dngroup -Members $user -Server $server -Confirm:$false
        $groups += $groupname
        }


    $pass = "Leaver" + ((0..9 | sort {Get-Random})[0..2] -join '')
    $password = ConvertTo-SecureString $pass -AsPlainText -Force

    Set-ADAccountPassword -Identity $user -Reset -NewPassword $password -Server $server 

    Set-ADUser -Identity $username -Replace @{msExchHideFromAddressLists=$true} -Server $server
    set-ADUser -Identity $username -Replace @{info="$($groups)"} -Server $server

    Disable-ADAccount -Identity $user -Server $server

    #$oldinfo = $user.info
    #$newinfo = 
    #Set-ADUser -Identity $username -Replace @{info="$($oldinfo) $($newinfo)"}

    Move-ADObject -Identity $userdn -TargetPath $disableddn -Server $server

    #}

