﻿Import-Module ActiveDirectory

$server = "HO-PPDC01.matches.com"
$importpath = "./UsersToFix.csv"
$emaildomain = "@matchesfashion.com"

$csv = Import-CSV -Path $importpath

$csv | ForEach-Object {
    
    $username = $_.username

    $user = Get-ADUser -Identity $username -properties proxyaddresses -server $server

    $firstname = $user.GivenName
    $lastname = $user.Surname


    $email = $firstname + "." + $lastname + $emaildomain
    $proxyaddress = "SMTP:" + $email


    Set-ADUser -Identity $username `
        -UserPrincipalName $email `
        -EmailAddress $email `
        -Add @{proxyAddresses = $proxyaddress } `
        -server $server

        Write-Host "Fixed $username"


            }