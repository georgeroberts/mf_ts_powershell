﻿$name = Read-Host "hostname"
$zone = Read-Host "zonename"
$oldobj = get-dnsserverresourcerecord -ComputerName ho-dc01 -name $name -zonename $zone -rrtype "A"
$newobj = get-dnsserverresourcerecord  -ComputerName ho-dc01 -name $name -zonename $zone -rrtype "A"
$updateip = Read-Host "new ip"
$newobj.recorddata.ipv4address=[System.Net.IPAddress]::parse($updateip)
Set-dnsserverresourcerecord -newinputobject $newobj -oldinputobject $oldobj -ComputerName ho-dc01 -zonename $zone -passthru 