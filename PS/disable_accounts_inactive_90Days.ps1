﻿Import-Module ActiveDirectory

$OU = "OU=DC,OU=Sites,DC=matches,DC=com"

$users = Search-ADAccount -SearchBase $OU -AccountInactive -TimeSpan ([timespan]90d) -UsersOnly

foreach ($user in $users) {

    if ($user.Enabled -eq $true) {

        Set-ADUser -Identity $user -Enabled $false -WhatIf

        } else {

        Write-Host "Account is disabled already"

    }