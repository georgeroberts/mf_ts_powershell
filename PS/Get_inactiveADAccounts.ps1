Import-Module ActiveDirectory

$ActiveOU = "OU=Sites,DC=matches,DC=com"

$inactiveusers = Get-ADUser -Filter * -SearchBase $ActiveOU -Properties Enabled, AccountExpirationDate, LastLogonDate | ? { `
($_.Enabled -EQ $False) -OR `
($_.AccountExpirationDate -NE $NULL -AND $_.AccountExpirationDate -LT (Get-Date)) -OR `
($_.LastLogonDate -NE $NULL -AND $_.LastLogonDate -LT (Get-Date).AddDays(-90)) }


$inactiveusers | select samaccountname, givenname, surname, enabled, accountexpirationdate, lastlogondate | Export-Csv -Path "c:\temp\all_inactiveusers.csv" -NoTypeInformation
