﻿Import-Module ActiveDirectory

$server = "HO-PPDC01.matches.com"

$username = Read-Host -Prompt 'Enter the Username of the Leaver to reverse'

#$csvpath = "C:\Users\karlns.adm\OneDrive - Matchesfashion.com\Documents\CSV\DCnewusers140818.csv"
#$csv = Import-CSV $csvpath

$disableddn = "OU=Disabled Users,OU=Disabled Accounts,DC=matches,DC=com"

#$csv | ForEach-Object {

    #$username = $_.samAccountName
    $user = Get-ADUser -Identity $username -Properties MemberOf,info -Server $server
    $groups = $user.info
    $userdn = $user.DistinguishedName


    $pass = "Matches" + ((0..9 | sort {Get-Random})[0..2] -join '')
    $password = ConvertTo-SecureString $pass -AsPlainText -Force

    Set-ADAccountPassword -Identity $user -Reset -NewPassword $password -Server $server 

    Set-ADUser -Identity $username -Replace @{msExchHideFromAddressLists=$false}

    Set-ADUser -Identity $userName -accountExpirationDate $null

    Enable-ADAccount -Identity $user -Server $server

    #$oldinfo = $user.info
    #$newinfo = 
    #Set-ADUser -Identity $username -Replace @{info="$($oldinfo) $($newinfo)"}

    #foreach ($group in $groups) 
        #{
        #Add-ADGroupMember -identity $group -Members $user -Server $server -Confirm:$false
        #}

    # Move-ADObject -Identity $userdn -TargetPath $disableddn -Server $server

    #}

    Write-Host -ForegroundColor Yellow "Username: $username Password: $pass"
    Write-Host -ForegroundColor Yellow "Manually Add users to previous groups. Groups can be found in Notes Section of AD"

