﻿$registryPath = "HKLM:\SOFTWARE\Policies\Microsoft\Windows\OneDrive"

$Name = "DisableFileSyncNGSC"

$value = "0"

IF(!(Test-Path $registryPath))

  {

    New-Item -Path $registryPath -Force | Out-Null

    New-ItemProperty -Path $registryPath -Name $name -Value $value `

  }

 ELSE {

    Set-ItemProperty -Path $registryPath -Name $name -Value $value
    }