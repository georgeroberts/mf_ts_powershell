﻿$hostname= (Invoke-WebRequest -UseBasicParsing 'http://169.254.169.254/latest/meta-data/local-ipv4').Content

if ($hostname.contains("ip-10-5-17-10")) {
    Rename-Computer -NewName "aws-vpnavts01" -Force -WhatIf
}
if ($hostname.contains("ip-10-5-17-70")) {
    Rename-Computer -NewName "aws-vpnavts02" -Force
}
if ($hostname.contains("ip-10-5-17-140")) {
    Rename-Computer -NewName "aws-vpnavts03" -Force
} 
if ($hostname.contains("ip-10-5-17-20")) {
   Rename-Computer -NewName "aws-vpnavts04" -Force
}

Restart-Computer