﻿Import-Module ActiveDirectory

$name = Read-Host -Prompt "OU Name"

New-ADOrganizationalUnit -Name $name -Path "OU=Head Office,OU=Desktop Computers,OU=Workstations,DC=matches,DC=com"
