Get-WMIObject -class Win32_UserProfile |
Where {(!$_.Special) -and ($_.ConvertToDateTime($_.LastUseTime) -lt (Get-Date).AddDays(0))} |
Remove-WmiObject -WhatIf
