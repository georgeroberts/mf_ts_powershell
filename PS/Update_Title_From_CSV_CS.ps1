﻿Import-Module ActiveDirectory

$ADServer = "HO-PPDC01"

$csv = Import-Csv C:\CSV\CXEmailTitles.csv
$csv | ForEach-Object { 
        $dn = "CN=" + $_.DisplayName + ",OU=Users,OU=Customer Care,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"

        Set-ADUser -Identity $dn -Title $_.Title -Description $_.Title -Department $_.Department -Server $ADServer
}
