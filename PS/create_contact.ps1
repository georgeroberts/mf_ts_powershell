﻿Import-Module ActiveDirectory

$name = Read-Host -Prompt 'Input users name'
$email = Read-Host -Prompt 'Input users email'
$description = Read-Host -Prompt 'Input description'

$path = "OU=Contacts,OU=Head Office,OU=Sites,DC=matches,DC=com"
$server = "HO-PPDC01.matches.com"
$proxyaddress = "SMTP:"+ $email

New-ADObject `
    -Type Contact `
    -Name $name `
    -DisplayName $name `
    -Path $path -OtherAttributes @{proxyaddresses = $proxyaddress} `
    -Server $server `
    -Description $description 