﻿$service = Read-Host "service name? i.e live"
$server = Read-Host "server name?"
Get-Service *$service* -exclude "liveupdate" -ComputerName $server | Restart-Service -Confirm -Verbose 