﻿Import-Module ActiveDirectory

$csvpath = "C:\Users\karlns.adm\OneDrive - Matchesfashion.com\Documents\CSV\svcaccounts280818pwtest.csv"

$users = Import-CSV -path $csvpath -Header "username"

$server = "HO-PPDC01"

foreach($user in $users){

    $username = $user.username
    $dn = (Get-ADUser $username -Server $server).DistinguishedName
    
    $password = -join ((48..57) + (65..90) + (97..122) | Get-Random -Count 15 | % {[char]$_})
    #Sets generated password as a secure password
    $pass = ConvertTo-SecureString $password -AsPlainText -Force

    Set-ADAccountPassword -Identity $dn -Reset -NewPassword $pass -Server $server

    Write-Host "The Password for $username is $password"
    
    }

    
