﻿Import-Module ActiveDirectory

$csv = Import-Csv -Path "./DCUserList.csv"
$server = "HO-PPDC01.matches.com"
$list = @() 

$csv | ForEach-Object {

$username = $_.username

#$user = Get-ADUser -Identity $username -properties ProxyAddresses,EmailAddress -Server $server 


$object = get-aduser $username -properties * -Server $server | select-object name, samaccountname, surname, emailaddress, enabled, @{"name"="proxyaddresses";"expression"={$_.proxyaddresses}} 
 

#$object = New-Object PSObject -Property @{
           # username = $user.SamAccountName
           # email = $user.EmailAddress
           # Proxy = $user.ProxyAddresses 
          #  }

            $list += $object 

}

$list | Export-csv -Path ".\UsersSyncdtoO365-new.csv" -NoTypeInformation
