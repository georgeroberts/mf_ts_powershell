﻿Function New-StandardUser{ 
        
    Param ([string]$site, [string]$department, [string]$firstname, [string]$lastname, [string]$username, [string]$title)

    try
    {
        if ($site -eq "HO")
        {
            $location = "OU=Users,OU=" + $department + ",OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
            $officename = "HO"
            Write-Host "Site set to Head Office"

        } elseif ($site -eq "DC") 
        {
            $location = "OU=Users,OU=" + $department + ",OU=DC,OU=Sites,DC=matches,DC=com"
            $officename = "DC"
            Write-Host "Site set to Distribution Centre"

        } elseif ($site -eq "HE")
        {
            $location = "OU=Users,OU=Here East,OU=Sites,DC=matches,DC=com"
            $officename = "HO"
            Write-Host "Site set to Here East"

        } elseif ($site -eq "HK")
        {
            $location = "OU=Users,OU=Hong Kong,OU=Sites,DC=matches,DC=com"
            $officename = "HO"
            Write-Host "Site set to Hong Kong"
        } elseif ($site -eq "SS")
        {
            $location = "OU=Users,OU="+ $department + ",OU=Stores,OU=Sites,DC=matches,DC=com"
        
            #To improve on current if statement for store signatire via the officename attribute in AD
        
            if ($department -eq "5 Carlos Place")
            {
                $officename = "CP"

            } elseif ($department -eq "36 Wimbledon")
            {
                $officename = "Wimbledon36"

            } elseif ($department -eq "Hackney")
            {
                $officename = "Hackney"

            }elseif ($department -eq "Ledbury 60")
            {
                $officename = "Ledbury (60)"

            }elseif ($department -eq "Marylebone")
            {
                $officename = "Marylebone (87)"

            }else
            {
                $officename = "HO"
               Write-Host "Site set to Stores but store is not listed, please check Office section for the correct signature!"

            }
            
        }  

    }
    catch
    {
        Write-Host "Location was not set correctly"
        Exit 1

    }


    #To enter details regarding new starter
    $firstname = Read-Host -Prompt 'Input users first name'
    $lastname = Read-Host -Prompt 'Input users surname'
    $username = Read-Host -Prompt 'Input the perferred username'
    $title = Read-Host -Prompt 'Input users title'


    #cleans up spaces in user inputs
    $firstname = $firstname.Trim()
    $lastname = $lastname.Trim()
    $username = $username.Trim()

    $firstemail = $firstname -replace '\s',''
    $lastemail = $lastname -replace '\s',''

    #Confirms that there are no foriegn characters in username

    $pat = "^[a-zA-Z]+$"

    if ($username -match $pat -and $firstemail -match $pat -and $lastemail -match $pat)
    {
        Write-Host "No issue with names"

    } else
    {
        Write-Host "Invalid character in Username, Exiting Script"
        Exit 1
    }

    #Set ups remaining variables

    #$dnsroot = (Get-ADDomain).DNSRoot
    $username = $username.ToLower()
    $emaildomain = "matchesfashion.com"
    $homepath = "\\aws-vplofs02.matches.com\Users$\" + $username
    $homedrive = "U:"
    $company = "Matches"
    #$officename = "HO"
    $proxyaddress = "SMTP:" + $firstemail + "." + $lastemail + "@" + $emaildomain
    $server = "HO-PPDC01.matches.com"

    #Sets Password for user account
    #$password = ([char[]]([char]33..[char]95) + ([char[]]([char]97..[char]126)) + 0..9 | sort {Get-Random})[0..8] -join ''
    $password = "Welcome" + ((0..9 | sort {Get-Random})[0..2] -join '')

    #Sets generated password as a secure password
    $pass = ConvertTo-SecureString $password -AsPlainText -Force


    #Sets Login Script and modifies the Office Attribute according to Department
    try 
    {
        if ($department -eq "Buying")
        {
            $script = "Buying.bat"
            Write-Host "Using $department script for new user"
        }
        elseif (($department -eq "Customer Care") -or ($deparment -eq "Private Shopping"))
        {
            $script = "MyStylist.bat"
            Write-Host "Using $department script for new user"
        }
        elseif ($department -eq "Editorial")
        {
            $script = "Editorial.bat"
            Write-Host "Using $department script for new user"
        }
        elseif ($department -eq "Finance")
        {
            $script = "Finance.bat"
            Write-Host "Using $department script for new user"
        }
        elseif ($department -eq "HR")
        {
            $script = "HR.bat"
            Write-Host "Using $department script for new user"
            $officename = "HR"
        }
        elseif ($department -eq "IT")
        {
            $script = "LoginIT.bat"
            Write-Host "Using $department script for new user"
        }
        elseif ($department -eq "Marketing")
        {
            $script = "Marketing.bat"
            Write-Host "Using $department script for new user"
        }
        elseif ($department -eq "Merchandising")
        {
            $script = "Merch.bat"
            Write-Host "Using $department script for new user"
        }
        elseif ($department -eq "Studio")
        {
            $script = "PhotoStudio.bat"
            Write-Host "Using $department script for new user"
        }
        elseif ($department -eq "RAEY")
        {
            $script = "login.bat"
            $officename = "RAEY"
            Write-Host "Set to use $officename Signature"
        }
        else
        {
            $script = "login.bat"
            Write-Host "Using default script for new user"
        }
    }
    catch
    { Write-Host "There was an issues with assigning a login script. Please do this manaully" } 

    #Checks if user already exists in AD
    try { $exist = Get-ADUser -LDAPFilter $username -Server $server}
    catch { }


    if (!$exist) 
    {
        try 
        {
            #Creates the user account in AD 

            Write-Host "Creating User Account..." 

            New-ADUser -name ($firstname + " " + $lastname) `
                -SamAccountName $username `
                -AccountPassword $pass `
                -GivenName $firstname `
                -Surname $lastname `
                -DisplayName ($firstname + " " + $lastname) `
                -Title $title `
                -Description $title `
                -Department $department `
                -UserPrincipalName ($firstemail + "." + $lastemail + "@" + $emaildomain) `
                -EmailAddress ($firstemail + "." + $lastemail + "@" + $emaildomain) `
                -HomeDirectory $homepath `
                -HomeDrive $homedrive `
                -ScriptPath $script `
                -Company $company `
                -Office $officename `
                -ChangePasswordAtLogon $true `
                -Enabled $true `
                -server $server


            #Pulls the Distinguished Name for the newly created AD account
            $dn = (Get-ADUser $username -Server $server).DistinguishedName

            # Adds Proxy address for Office 365 Sync
            try { $dn | Set-ADUser -Add @{proxyAddresses = $proxyaddress } -ErrorAction Stop -Server $server}
            catch { Write-Host "Could not create the proxy address $proxyaddress in attributes. Please perform this manually" }

            #Confirms OU location exists in AD before moving account
            if ([adsi]::Exists("LDAP://$($location)"))
            {
        
            Move-ADObject -Identity $dn -TargetPath $location -Server $server

            }
            Else
            {
            Write-Host "Location was not found, please move account manually"
            }
        
            #Section to add user to groups
        
        
        
        
        
            #Feeds back user details to TSA

            Write-Host "The User Account for $firstname $lastname has been created."
            Write-Host "Log in details is as follows:"
            Write-Host "Username: $username"
            Write-Host "Password: $password"
            Write-Host "Email: $firstemail.$lastemail@$emaildomain"
            Write-Host "The User's Password is set to change on their first login"
            Write-Host "Please add user to groups via AD" 

        }
        Catch
        {
            Write-Host "Something Went Wrong"
        }

    } 
    else 
    {
        Write-Host "The User $username already exists! Please try another username"
    }
}