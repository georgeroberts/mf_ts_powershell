﻿# Import active directory module for running AD cmdlets
Import-module ActiveDirectory

#Store the data from UserList.csv in the $List variable
$List = Import-CSV "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\newusers.csv"

#Loop through user in the CSV
ForEach ($User in $List)
{

$username = $User.username
$site = $user.site
$department = $user.department

#Add the user to the TestGroup1 group in AD

#Add-ADGroupMember -Identity DC_Packers -Members $User.username
if ($site -eq "HO") {
    
    Add-ADGroupMember -Identity sec_navprd_tsrdp -Members $username
    Add-ADGroupMember -Identity sec_litmos_learners -Members $username
    Add-ADGroupMember -Identity Sec_OktaFairsail -Members $username

    if ($department -eq "Customer Care") {
        Add-ADGroupMember -Identity dep_CustomerCare -Members $username
        Add-ADGroupMember -Identity sec_Eshop -Members $username
        }
        elseif ($department -eq "IT") {
        Add-ADGroupMember -Identity dep_IT -Members $username
        Add-ADGroupMember -Identity MatchesIT -Members $username
        Add-ADGroupMember -Identity "Sonicwall SSLVPN" -Members $username
        }

    Write-Host -ForegroundColor Green "User $username completed"
    } else {
    Write-Host -ForegroundColor Red "Please Manually Add the user $username to Groups in Active Directory"
    }

}