﻿Import-module ActiveDirectory

$csvimport = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\dcuserssearch.csv"
$csvexport = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\dcusersresults.csv"

$csv = import-csv -path $csvimport

$array = @()

$csv | ForEach-Object {

    $username = $_.username
    $firstname = $_.firstname
    $lastname = $_.lastname

    $user = get-aduser -Identity $username | select samaccountname,givenname,surname,enabled,lastlogon

    if (!$user) {
        $user = get-aduser -Filter {givenname -eq $givenname -and surname -eq $surname} | select samaccountname,givenname,surname,enabled,lastlogon
    }

    if(!$user) {

        $credentials = New-Object PSObject -Property @{
                Firstname = $firstname
                Lastname = $lastname
                Username = $username
                Exists = "N"
                Disabled = "NA"
                LastLogon = "NA"

                }
            $array += $credentials

    } else {

        $credentials = New-Object PSObject -Property @{
                Firstname = $user.givenname
                Lastname = $user.surname
                Username = $user.samaccountname
                Exists = "Y"
                Disabled = $user.enabled
                LastLogon = $user.lastlogon

                }
            $array += $credentials
            }
}

$array | Export-csv -Path $csvexport