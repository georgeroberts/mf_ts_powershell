﻿Import-Module ActiveDirectory

$importpath = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\importusers2.csv"
$exportpath = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\exportuserdc.csv"

$csv = import-csv -Path $importpath


$users = @()
$failure = "NA"

foreach ($line in $csv) {

    $givenname = $line.GivenName
    $surname = $line.Surname
    
    $name = $givenname + " " + $surname

    $search = get-aduser -Filter {givenname -eq $givenname -and surname -eq $surname}

    if(($search.givenname -eq $givenname) -and ($search.surname -eq $surname)) {

        $results = New-Object PSObject -Property @{
                GivenName = $givenname
                Surname = $surname
                Username = $search.samaccountname
                }
          $users += $results
    } else {
        $results = New-Object PSObject -Property @{
                GivenName = $givenname
                Surname = $surname
                Username = $failure
                }
          $users += $results
    }

}

$users | export-csv $exportpath