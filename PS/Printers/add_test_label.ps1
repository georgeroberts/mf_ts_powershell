﻿#pnputil /add-driver C:\Printer\ZD5-1-16-6854\ZBRN\ZBRN.inf
#Add-PrinterDriver -Name "ZDesigner GK420d"

$csv = Import-Csv c:\Printer\test_label_printers.csv
$csv | ForEach-Object {
  Add-PrinterPort -Name $_.printername -PrinterHostAddress $_.ipaddress
  Add-Printer -Name $_.PrinterName -DriverName $_.driver -PortName $_.printername -Comment $_.department  -Location $_.location -Shared -ShareName $_.printername
}