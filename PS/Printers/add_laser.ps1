﻿#pnputil /add-driver C:\Printer\HP_LaserJet_Pro_M402-M403_n-dne\hpdo602a_x64.inf
#Add-PrinterDriver -Name "HP LaserJet Pro M402-M403 n-dne PCL 6"

$csv = Import-Csv c:\Printer\laser_printers.csv
$csv | ForEach-Object {
  Add-PrinterPort -Name $_.printername -PrinterHostAddress $_.ipaddress
  Add-Printer -Name $_.PrinterName -DriverName $_.driver -PortName $_.printername -Comment $_.department  -Location $_.location -Shared -ShareName $_.printername
}