﻿# Import active directory module for running AD cmdlets
Import-module ActiveDirectory

#Store the data from UserList.csv in the $List variable
$List = Import-CSV "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\updatedcusers.csv"
$array = @()
$server = "HO-PPDC01.matches.com"

$csvexport = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\updatedcuserslog.csv"

#Loop through user in the CSV
ForEach ($User in $List)
{

$username = $User.username
$id = $user.employeeid

$username = $username.Trim()
$username = $username -replace '\s',''


#Add-ADGroupMember -Identity DC_Packers -Members $User.username
    try{
        Add-ADGroupMember -Identity sec_nav_stocktake -Members $username -server $server
        Set-ADUser -Identity $username -EmployeeID $id
        Write-host -forgroundcolor Green "Group and employee id updated for $username"
        $credentials = New-Object PSObject -Property @{
                    Username = $username
                    Completed = "Y"

                    }
                $array += $credentials

    }catch {
        Write-host -ForegroundColor Red "Group and employee id failed for $username"
        $credentials = New-Object PSObject -Property @{
                    Username = $username
                    Completed = "N"
                    }
    }

}

$array | Export-CSV -Path $csvexport