﻿Import-module ActiveDirectory

$csvimport = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\dcuserssearch.csv"
$csvexport = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\dcusersresults.csv"

$csv = import-csv -path $csvimport

$location = "OU=Users,OU=StockTake,OU=DC,OU=Sites,DC=matches,DC=com"
$department = "StockTake"
$site = "DC"
$title = "Warehose Operative"
$company = "matches"

$group = "sec_nav_stocktake"
$expirationdate = "01/03/2019  10:00PM"

$array = @()

$csv | ForEach-Object {

    $username = $_.username
    $id = $_.employeei

    $password = "Welcome" + ((0..9 | sort {Get-Random})[0..2] -join '')


    #Sets generated password as a secure password
    $pass = ConvertTo-SecureString $password -AsPlainText -Force

    Move-ADObject -Identity $username -TargetPath $location -Server $server
    
    Set-ADUser -Identity $username `
        -Enabled $true `
        -Department $department `
        -Office $site `
        -EmployeeID $id `
        -Title $title `
        -Description $title `
        -Department $department `
        -Office $site `
        -ChangePasswordAtLogon $false `
        -company $company `
        -server $server

    Set-ADAccountPassword -Identity $username -NewPassword $pass -Server $server

     #Set the account expiry date
     Set-ADAccountExpiration -Identity $username -DateTime $expirationdate -Server $server

     #Add user to stocktake group
     Add-ADGroupMember -Identity $group -Members $username -server $server

      $credentials = New-Object PSObject -Property @{
                Firstname = $firstname
                Lastname = $lastname
                Username = $username
                Middlename = $middlename
                Password = $password
                Created = "Y"
                Employeeid = $id

                }
            $array += $credentials

}

$csv | export-csv -Path $csvexport