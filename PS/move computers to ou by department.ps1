﻿#Import AD module
Import-Module ActiveDirectory

#Old Location for Machines
$oldOUDN = "OU=Workstations,DC=matches,DC=com"

#OU Distinguished Names for each Department
#Head Office Locations
$itDN = "OU=Computers,OU=IT,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$buyingDN = "OU=Computers,OU=Buying,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$merchandisingDN = "OU=Computers,OU=Merchandising,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$pressDN = "OU=Computers,OU=Press,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$eventsDN = "OU=Computers,OU=Events,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$customercareDN = "OU=Computers,OU=Customer Care,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$privateshoppingDN = "OU=Computers,OU=Private Shopping,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$editorialDN = "OU=Computers,OU=Editorial,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$marketingDN = "OU=Computers,OU=Marketing,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$hrDN = "OU=Computers,OU=HR,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$financeDN = "OU=Computers,OU=Finance,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$logisticsDN = "OU=Computers,OU=Logistics,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$productDN = "OU=Computers,OU=Product,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$raeyDN = "OU=Computers,OU=RAEY,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
$executivesDN = "OU=Computers,OU=Executives,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"
#Here East Locations
$hereeastDN = "OU=Computers,OU=Here East,OU=Sites,DC=matches,DC=com"

#Log Files
$time = Get-Date -UFormat %d%m%y_%H%M
$logpath = "C:\temp\computermove_$time.csv"

#Other Variables
$server = "HO-PPDC01.matches.com"

#Pulls all machines in Old location to a variable
$computers = Get-ADComputer -Filter * -SearchBase $oldOUDN -Server $server

#Moves machines to OU based on Naming Convention
$logs = foreach ($computer in $computers) {
    
    #Sets department to unknown. this is set to the department as long as the machine is found.
    $department = "unknown"

    #Move machine as per naming convention
    
    if (($computer.Name -like "UKHOITLT*") -Or ($computer.Name -like "IT-LT*") -Or ($computer.Name -like "UKHOITPC*") -Or ($computer.Name -like "IT-PC*")) 
    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $itDN -Server $server -WhatIf
        $department = "IT"
    } 
    elseif (($computer.Name -like "UKHOBULT*") -Or ($computer.Name -like "BU-LT*") -Or ($computer.Name -like "UKHOBUPC*") -Or ($computer.Name -like "BU-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $buyingDN -Server $server -WhatIf
        $department = "Buying"
    }
    elseif (($computer.Name -like "UKHOFILT*") -Or ($computer.Name -like "FI-LT*") -Or ($computer.Name -like "UKHOFIPC*") -Or ($computer.Name -like "FI-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $financeDN -Server $server -WhatIf
        $department = "Finance"
    }
    elseif (($computer.Name -like "UKHOMELT*") -Or ($computer.Name -like "ME-LT*") -Or ($computer.Name -like "UKHOMEPC*") -Or ($computer.Name -like "ME-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $merchandisingDN -Server $server -WhatIf
        $department = "Merchandising"
    }
    elseif (($computer.Name -like "UKHOHRLT*") -Or ($computer.Name -like "HR-LT*") -Or ($computer.Name -like "UKHRBUPC*") -Or ($computer.Name -like "HR-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $hrDN -Server $server -WhatIf
        $department = "HR"
    }
    elseif (($computer.Name -like "UKHOMKLT*") -Or ($computer.Name -like "MK-LT*") -Or ($computer.Name -like "UKHOMKPC*") -Or ($computer.Name -like "MK-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $marketingDN -Server $server -WhatIf
        $department = "Marketing"
    }
    elseif (($computer.Name -like "UKHOEXLT*") -Or ($computer.Name -like "EX-LT*") -Or ($computer.Name -like "UKEXBUPC*") -Or ($computer.Name -like "EX-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $executivesDN -Server $server -WhatIf
        $department = "Exec"
    }
    elseif (($computer.Name -like "UKHOEDLT*") -Or ($computer.Name -like "ED-LT*") -Or ($computer.Name -like "UKHOEDPC*") -Or ($computer.Name -like "ED-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $editorialDN -Server $server -WhatIf
        $department = "Editorial"
    }
    elseif (($computer.Name -like "UKHORALT*") -Or ($computer.Name -like "RA-LT*") -Or ($computer.Name -like "UKHORAPC*") -Or ($computer.Name -like "RA-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $raeyDN -Server $server -WhatIf
        $department = "RAEY"
    }
    elseif (($computer.Name -like "UKHOCSLT*") -Or ($computer.Name -like "CS-LT*") -Or ($computer.Name -like "UKHOCSPC*") -Or ($computer.Name -like "CS-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $customercareDN -Server $server -WhatIf
        $department = "Eshop"
    }
    elseif (($computer.Name -like "UKHOPRLT*") -Or ($computer.Name -like "PR-LT*") -Or ($computer.Name -like "UKHOPRPC*") -Or ($computer.Name -like "PR-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $pressDN -Server $server -WhatIf
        $department = "Press"
    }
    elseif (($computer.Name -like "UKHOPSLT*") -Or ($computer.Name -like "PS-LT*") -Or ($computer.Name -like "UKHOPSPC*") -Or ($computer.Name -like "PS-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $privateshoppingDN -Server $server -WhatIf
        $department = "Private Shopping"
    }
    elseif (($computer.Name -like "UKHOEVLT*") -Or ($computer.Name -like "EV-LT*") -Or ($computer.Name -like "UKHOEVPC*") -Or ($computer.Name -like "EV-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $eventsDN -Server $server -WhatIf
        $department = "Events"
    }
    elseif (($computer.Name -like "UKHOLSLT*") -Or ($computer.Name -like "LS-LT*") -Or ($computer.Name -like "UKHOLSPC*") -Or ($computer.Name -like "LS-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $logisticsDN -Server $server -WhatIf
        $department = "Logistics"
    }
    elseif (($computer.Name -like "UKHOPDLT*") -Or ($computer.Name -like "PD-LT*") -Or ($computer.Name -like "UKHOPDPC*") -Or ($computer.Name -like "PD-PC*")) 

    {
        Move-ADObject -Identity $computer.DistinguishedName -TargetPath $productDN -Server $server -WhatIf
        $department = "Product"
    }

    #Add machine to logs

    New-Object PSObject -Property @{
            Hostname = $computer.Name
            Department = $department
            }
}

#Exports logs to CSV

$logs | Export-Csv $logpath