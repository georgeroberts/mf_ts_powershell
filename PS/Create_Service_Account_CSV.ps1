﻿#########################################################################################
# Created By Karl Newman-Smart (karlns)
# Script to Create new users within MatchesFashion using CSV file 
# Please Check CSV path before running!
#
# Ver 1.0
# - Imports modules and captures user information
# - Creates user in HO OU with all standard attributes filled in AD
# - Moves user to correct OU according to department for HO
# - Confirms location is valid before moving, user account is created in global default OU
# - Sets proxy address once user account is created
# - Sets login script according to department
# Ver 1.1
# - Expanded to Create users in HO, DC, HE and HK OUs in AD
# - Cleans up spaces in first and last name
# - Sets Office Attribute according to Site
# - Sets to generate 9 character password
# - Enables account and sets user to change password on first logon
# Ver 1.2
# - Changed output to one line per user 
# - Commented out several Write-Host lines
# - 
#########################################################################################

#Loads Active Directory Module. Script will stop if this module cannot be loaded.
 
Try
{
    Import-Module ActiveDirectory
}
Catch
{
    Write-Host "ActiveDirectory Module could not be loaded. Script has Stopped"
    Exit 1
}

#Sets the site according to user input, exits script if this cannot be done
$csvpath = "C:\Users\karlns.adm\OneDrive - Matchesfashion.com\Documents\CSV\svcaccounts280818.csv"
#$outputPath = "C:\temp\svc_accounts.csv"

$users = Import-CSV -Path $csvpath
$log = "C:\Temp File\createdusers.txt"

$location = "OU=Test,OU=Tester,OU=DC,OU=Sites,DC=matches,DC=com"




ForEach ($user in $users) {

    $username =$user.username
    $username = $username.Trim()

    $username = $username.ToLower()
    $description = $user.description
    $emaildomain = "matchesfashion.com"
    
    $server = "HO-PPDC01"

    #Sets Password for user account
    $password = ([char[]]([char]33..[char]95) + ([char[]]([char]97..[char]126)) + 0..9 | sort {Get-Random})[0..8] -join ''


    #Sets generated password as a secure password
    $pass = ConvertTo-SecureString $password -AsPlainText -Force

    

    #Checks if user already exists in AD
    try { $exist = Get-ADUser -LDAPFilter $username -Server $server}
    catch { }


    if (!$exist) 
    {
            #Creates the user account in AD 

            #Write-Host "Creating User Account..." 

            New-ADUser -name $username `
                -SamAccountName $username `
                -AccountPassword $pass `
                -DisplayName $username `
                -Description $description `
                -UserPrincipalName ($username + "@" + $emaildomain) `
                -EmailAddress ($username + "@" + $emaildomain) `
                -ChangePasswordAtLogon $false `
                -Enabled $true `
                -server $server


            #Pulls the Distinguished Name for the newly created AD account
            $dn = (Get-ADUser $username -Server $server).DistinguishedName
            
            #Confirms OU location exists in AD before moving account
            if ([adsi]::Exists("LDAP://$($location)"))
            {
        
            Move-ADObject -Identity $dn -TargetPath $location -Server $server

            }
            Else
            {
            #Write-Host "Location was not found, please move account manually"
            }
            # Feeds back user details to TSA

            Write-Host "The Service Account $username as been created. Username is $username. Password is $password" 


    } 
    else 
    {
        Write-Host "Service Account creation failed = $username"
    }

}