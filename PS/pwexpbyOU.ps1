﻿Get-ADUser -SearchBase "OU=head office,OU=sites,DC=matches,DC=com" -filter {Enabled -eq $True -and PasswordNeverExpires -eq $False} -Properties "SamAccountName","msDS-UserPasswordExpiryTimeComputed" | 
  Select-Object -Property "SamAccountName", @{Name="Password Expiry Date"; Expression={[datetime]::FromFileTime($_."msDS-UserPasswordExpiryTimeComputed")}} |
    Export-Csv C:\Temp\pwexpHO.csv