﻿Import-Module ActiveDirectory

$csvpath = "C:\Users\karlns.adm\OneDrive - Matchesfashion.com\Documents\CSV\DCnewusers140818.csv"

$csv = Import-CSV $csvpath

$disableddn = "OU=Disabled Users,OU=Disabled Accounts,DC=matches,DC=com"

$csv | ForEach-Object {

    $username = $_.samAccountName
    $user = Get-ADUser -Identity $username -Properties MemberOf
    $groups = $user.MemberOf
    $userdn = $user.DistingushedName


    $pass = "Welcome" + ((0..9 | sort {Get-Random})[0..2] -join '')
    $password = ConvertTo-SecureString $pass -AsPlainText -Force

    Set-ADAccountPassword -Identity $username -Reset -NewPassword $password

    Disable-ADAccount -Identity $username

    #$oldinfo = $user.info
    #$newinfo = 
    #Set-ADUser -Identity $username -Replace @{info="$($oldinfo) $($newinfo)"}

    foreach ($group in $groups) 
        {
        Remove-ADGroupMember -identity $group -Members $username
        }

    Move-ADObject -Identity $userdn -TargetPath $disableddn

    }

