﻿Import-Module ActiveDirectory

$importcsv = "./userlist.csv"
$exportcsv = "./user_passwords.csv"
$server = "HO-PPDC01.matches.com"

$userpasswordlist = @()

$users = Import-csv $importcsv

$users | ForEach-Object {

    $username = $_.username
    $name = (Get-ADUser $_.username -Server $server).name
    $email= (Get-ADUser $_.username -Properties EmailAddress -Server $server).EmailAddress

    $pass = "Welcome" + ((0..9 | sort {Get-Random})[0..2] -join '')
    $password = ConvertTo-SecureString $pass -AsPlainText -Force

    Set-ADAccountPassword -Identity $username -Reset -NewPassword $password -Server $server
    Set-ADUser $username -ChangePasswordAtLogon $false

    $userpasswordobject = New-Object PSObject -Property @{
            Name = $name
            Email = $email
            Username = $username 
            Password = $pass
            
            }
          $userpasswordlist += $userpasswordobject

          }

$userpasswordlist | Export-Csv $exportcsv


