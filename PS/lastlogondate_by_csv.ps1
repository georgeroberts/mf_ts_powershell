﻿Import-Module ActiveDirectory

$importpath = "C:\Users\karlns.adm\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\account verification copy.csv"
$exportpath = "C:\temp\lastsetpasswords.csv"

$users = Import-Csv -Path $importpath

$userarray = @()

$users | ForEach-Object {
    
    $email = $_.email
    $filter = "UserPrincipalName -eq '$email'"

    $info = Get-ADUser -Filter $filter -Properties PasswordLastSet, Displayname, Enabled, LastLogonDate

        $userinfo = New-Object PSObject -Property @{
            Username = $info.samAccountName
            PasswordLast = $info.PasswordLastSet
            Enabled = $info.Enabled
            Name = $info.Displayname
            LastLogon = $info.LastLogon
            }
      $userarray += $userinfo
    }

    $userarray | Export-csv -Path $exportpath