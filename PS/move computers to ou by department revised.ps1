﻿#Import AD module
Import-Module ActiveDirectory

#Old Location for Machines
$oldOUDN = "OU=Workstations,DC=matches,DC=com"
$testOU = "OU=Computers,OU=IT,OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"

#Log Files
$time = Get-Date -UFormat %d%m%y_%H%M
$logpath = "C:\temp\computermove_$time.csv"
$errorpath ="C:\temp\computererror_$time.csv"

#Other Variables
$server = "HO-PPDC01.matches.com"

#Pulls all machines in Old location to a variable
$computers = Get-ADComputer -Filter * -SearchBase $testOU -Server $server

$computerarray = @()
$computererror = @()

#Moves machines to OU based on Naming Convention
foreach ($computer in $computers) {
    
    #Sets department to unknown. this is set to the department as long as the machine is found.
    $compname = $computer.Name
    $department = "unknown"
    $type = "unknown"
    $site = "unknown"

    #Move machine as per naming convention

    #Divides machines to WorkStations and Laptops

    if (($computer.Name.contains("PC") -eq "True") -or ($computer.Name.contains("pc") -eq "True"))
    {
        $type = "WorkStation"
    } 
    elseif (($computer.Name.contains("LT") -eq "True") -or ($computer.Name.contains("lt") -eq "True"))
    {
        $type = "Laptop"
    }

    #Divides machines to Site 

    if(($computer.Name.contains("HO") -eq "True") -or ($computer.Name.contains("ho") -eq "True")) 
    {
        $site = "Head Office"
    } 
    elseif (($computer.Name.contains("HE") -eq "True") -or ($computer.Name.contains("he") -eq "True")) 
    {
        $site = "Head Office"
    } 
    elseif (($computer.Name.contains("DC") -eq "True") -or ($computer.Name.contains("he") -eq "True")) 
    {
        $site = "DC"
    } 

    #Divides machines to Department

    if ( ($computer.Name.contains("IT") -eq "True") -or ($computer.Name.contains("it") -eq "True") ) 
    {
        $department = "IT"
    } 
    elseif ( ($computer.Name.contains("BU") -eq "True") -or ($computer.Name.contains("bu") -eq "True") ) 
    {
        $department = "Buying"
    }
    elseif ( ($computer.Name.contains("FI") -eq "True") -or ($computer.Name.contains("fi") -eq "True") ) 
    {
        $department = "Finance"
    }
    elseif ( ($computer.Name.contains("ME") -eq "True") -or ($computer.Name.contains("me") -eq "True") ) 
    {
        $department = "Merchandising"
    }
    elseif ( ($computer.Name.contains("HR") -eq "True") -or ($computer.Name.contains("hr") -eq "True") ) 
    {
        $department = "HR"
    }
    elseif ( ($computer.Name.contains("MK") -eq "True") -or ($computer.Name.contains("mk") -eq "True") ) 
    {
        $department = "Marketing"
    }
    elseif ( ($computer.Name.contains("EX") -eq "True") -or ($computer.Name.contains("ex") -eq "True") ) 
    {
        $department = "Executives"
    }
    elseif ( ($computer.Name.contains("ED") -eq "True") -or ($computer.Name.contains("ed") -eq "True") ) 
    {
        $department = "Editorial"
    }
    elseif ( ($computer.Name.contains("RA") -eq "True") -or ($computer.Name.contains("ra") -eq "True") ) 
    {
        $department = "RAEY"
    }
    elseif ( ($computer.Name.contains("CS") -eq "True") -or ($computer.Name.contains("cs") -eq "True") ) 
    {
        $department = "Customer Care"
    }
    elseif ( ($computer.Name.contains("PS") -eq "True") -or ($computer.Name.contains("ps") -eq "True") ) 
    {
        $department = "Private Shopping"
    }
    elseif ( ($computer.Name.contains("PR") -eq "True") -or ($computer.Name.contains("pr") -eq "True") ) 
    {
        $department = "Press"
    }
    elseif ( ($computer.Name.contains("EV") -eq "True") -or ($computer.Name.contains("ev") -eq "True") ) 
    {
        $department = "Events"
    }
    elseif ( ($computer.Name.contains("LS") -eq "True") -or ($computer.Name.contains("ls") -eq "True") ) 
    {
        $department = "Logistics"
    }
    elseif ( ($computer.Name.contains("PD") -eq "True") -or ($computer.Name.contains("pd") -eq "True") ) 
    {
        $department = "Product"
    }

    #Seperates out Macs

    if (($computer.Name.contains("mb") -eq "True") `
    -or ($computer.Name.contains("md") -eq "True") `
    -or ($computer.Name.contains("mlt") -eq "True") `
    -or ($computer.Name.contains("imac") -eq "True") `
    -or ($computer.Name.contains("mpc") -eq "True"))
    {
        $type = "Mac"
    }

    #Add machine to lists

    if(($department -eq "unknown") -or ($type -eq "unknown") -or ($type -eq "Mac")){

    $computererrorlist = New-Object PSObject -Property @{
            Hostname = $compname
            Department = $department
            Type = $type
            Site = $site
            }
      $computererror += $computererrorlist
    } 
    else 
    {

    $arraylist = New-Object PSObject -Property @{
            Hostname = $compname
            Department = $department 
            Type = $type
            Site = $site
            }
      $computerarray += $arraylist
    }
}

foreach ($item in $computerarray) {

    $pcdn = Get-ADComputer -Identity $item.Hostname -Server $server
    $targetdn = "OU=" + $item.Type + ",OU=Computers,OU=" + $item.Department + ",OU=Departments,OU=Head Office,OU=Sites,DC=matches,DC=com"        
    Move-ADObject -Identity $pcdn -TargetPath $targetdn -Server $server
    }

#Exports logs to CSV

$computerarray | Export-Csv $logpath
$computererror | Export-Csv $errorpath