﻿Import-Module ActiveDirectory

$importpath = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\importuser.csv"
$exportpath = "C:\Users\karlns\OneDrive - Matchesfashion.com\dev\mf_ts_powershell\PS\exportuser.csv"

$csv = import-csv -Path $importpath


$users = @()
$failure = "NA"

foreach ($line in $csv) {

    $givenname = $line.GivenName
    $surname = $line.Surname

    $name = $givenname + " " + $surname

    $search = get-aduser -Filter {givenname -eq $givenname }
    
    if($search.name -eq $name) {

    Write-Host $search , $search.samaccountname
    }


    }