Import-Module ActiveDirectory
$groups = Import-Csv ‘c:\scripts\groups-dev10.csv‘
foreach ($group in $groups) {
New-ADGroup -Name $group.name -Path “OU=DEV10,OU=DEV,OU=Platform Security Groups,DC=matches,DC=com”  -GroupCategory Security -GroupScope Universal
}
